package sonha.com.controller;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sonha.com.contrains.Contrains;
import sonha.com.contrains.TicketSellingException;
import sonha.com.dao.ChoNgoiLichChieuDAO;
import sonha.com.dao.HoaDonDAO;
import sonha.com.dao.KhachHangDAO;
import sonha.com.dao.LichChieuDAO;
import sonha.com.dao.PhimDAO;
import sonha.com.dao.PhongChieuDAO;
import sonha.com.dao.VeDAO;
import sonha.com.model.ChoNgoiLichChieu;
import sonha.com.model.HoaDon;
import sonha.com.model.KhachHang;
import sonha.com.model.KhungGio;
import sonha.com.model.LichChieu;
import sonha.com.model.NhanVien;
import sonha.com.model.Phim;
import sonha.com.model.PhongChieu;
import sonha.com.model.RapPhim;
import sonha.com.model.Ve;

/*
 *@Author: sonha
 *@Date: Nov 7, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Controller
public class BanVeController {
	@Autowired
	private PhimDAO phimDAO;
	@Autowired
	private PhongChieuDAO phongChieuDAO;
	@Autowired
	private LichChieuDAO lichChieuDAO;
	@Autowired
	private ChoNgoiLichChieuDAO choNgoiLIchChieuDAO;
	@Autowired
	private KhachHangDAO khachHangDAO;
	@Autowired
	private HoaDonDAO hoaDonDAO;
	@Autowired
	private VeDAO veDAO;

	@GetMapping("/sell")
	public String showSellingView(HttpServletRequest request, Model model) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");
			List<Phim> phims = phimDAO.getAll();
			model.addAttribute("phims", phims);
			return "GDBanVe";
		} else {
			return null;
		}
	}

	@GetMapping("/selectByFilm")
	public String selectByFilm(HttpServletRequest request, Model model) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");
			List<Phim> phims = phimDAO.getAll();
			model.addAttribute("phims", phims);
			return "GDChonPhim";
		} else {
			return null;
		}
	}

	@GetMapping("/selectByRoom")
	public String selectByRoom(HttpServletRequest request, Model model) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");
			List<PhongChieu> phongChieus = phongChieuDAO.getAll();
			model.addAttribute("phongChieus", phongChieus);
			return "GDChonPhong";
		} else {
			return null;
		}
	}

	@PostMapping("/showtimePaging")
	public String showtimePaging(HttpServletRequest request, Model model,
			@RequestParam(value = "filmId", defaultValue = "0") Integer idPhim,
			@RequestParam(value = "roomId", defaultValue = "0") Integer idPhong) {
		if (idPhim != 0) {
			Phim phim = phimDAO.get(idPhim);
			NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
			if (nhanVienLogined != null) {
				int totalRecord = lichChieuDAO.totalRecordByPhim(phim);
				int pageSize = Contrains.PAGE_SIZE;
				int totalPage = Integer.parseInt(request.getParameter("totalPage"));
				int oldPage = Integer.parseInt(request.getParameter("oldPage"));
				int newPage = Integer.parseInt(request.getParameter("newPage"));
				int position = (newPage - 1) * pageSize;
				List<LichChieu> lichChieus = lichChieuDAO.getLichChieuByPhim(position, pageSize, phim);
				model.addAttribute("lichChieus", lichChieus);
				request.setAttribute("totalPage", totalPage);
				request.setAttribute("newPage", newPage);
				return "GDChonLichChieu";
			} else {
				return null;
			}
		}
		if (idPhong != 0) {
			PhongChieu phongChieu = phongChieuDAO.get(idPhong);
			NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
			if (nhanVienLogined != null) {
				int totalRecord = lichChieuDAO.totalRecordByPhong(phongChieu);
				int pageSize = Contrains.PAGE_SIZE;
				int totalPage = Integer.parseInt(request.getParameter("totalPage"));
				int oldPage = Integer.parseInt(request.getParameter("oldPage"));
				int newPage = Integer.parseInt(request.getParameter("newPage"));
				int position = (newPage - 1) * pageSize;
				List<LichChieu> lichChieus = lichChieuDAO.getLichChieuByPhongChieu(position, pageSize,
						phongChieu);
				model.addAttribute("lichChieus", lichChieus);
				request.setAttribute("totalPage", totalPage);
				request.setAttribute("newPage", newPage);
				return "GDChonLichChieu";
			} else {
				return null;
			}
		}
		return null;
	}

	@PostMapping("/showtimeByRoom")
	public String showtimeByRoom(HttpServletRequest request, Model model,
			@RequestParam(value = "roomId") Integer idPhong) {
		PhongChieu phongChieu = phongChieuDAO.get(idPhong);
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			int totalRecord = lichChieuDAO.totalRecordByPhong(phongChieu);
			int pageSize = Contrains.PAGE_SIZE;
			int totalPage = 1;
			if (totalRecord % pageSize != 0) {
				totalPage = totalRecord / pageSize + 1;
			} else {
				totalPage = totalRecord / pageSize;
			}

			List<LichChieu> lichChieus = lichChieuDAO.getLichChieuByPhongChieu(totalPage, pageSize,
					phongChieu);
			model.addAttribute("lichChieus", lichChieus);
			request.setAttribute("totalPage", totalPage);
			request.setAttribute("newPage", 1);
			return "GDChonLichChieu";
		} else {
			return null;
		}
	}

	@PostMapping("/showtimeByFilm")
	public String showtimeByFilm(HttpServletRequest request, Model model,
			@RequestParam(value = "filmId") Integer idPhim) {
		Phim phim = phimDAO.get(idPhim);
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			int totalRecord = lichChieuDAO.totalRecordByPhim(phim);
			int pageSize = Contrains.PAGE_SIZE;
			int totalPage = 1;
			if (totalRecord % pageSize != 0) {
				totalPage = totalRecord / pageSize + 1;
			} else {
				totalPage = totalRecord / pageSize;
			}

			List<LichChieu> lichChieus = lichChieuDAO.getLichChieuByPhim(0, pageSize, phim);
			model.addAttribute("lichChieus", lichChieus);
			request.setAttribute("totalPage", totalPage);
			request.setAttribute("newPage", 1);
			return "GDChonLichChieu";
		} else {
			return null;
		}
	}

	@PostMapping("/selectSeat")
	public String selectSeat(HttpServletRequest request, Model model,
			@RequestParam(value = "scheduleId") Integer idLich) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");

			LichChieu lichChieu = lichChieuDAO.get(idLich);
			PhongChieu phongChieu = lichChieu.getPhongChieu();
			List<ChoNgoiLichChieu> choNgoiLichChieus = choNgoiLIchChieuDAO.getAllByLichChieu(lichChieu);
			model.addAttribute("choNgoiLichChieus", choNgoiLichChieus);
			model.addAttribute("phongChieu", phongChieu);
			request.setAttribute("choNgoiLichChieus", choNgoiLichChieus);
			request.setAttribute("phongChieu", phongChieu);

			return "GDChonCho";
		} else {
			return null;
		}
	}

	@PostMapping("/comfirmTicket")
	public String confirmTicket(HttpServletRequest request,
			@RequestParam(value = "id") Integer idPhong,
			@RequestParam(value = "listSeatId[]") Integer[] dsIdChoNgoiLichChieu, Model model) throws TicketSellingException {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			HttpSession session = request.getSession();
			List<ChoNgoiLichChieu> choNgoiLichChieus = new ArrayList<>();
			List<Ve> ves = new ArrayList<Ve>();
			PhongChieu phongChieu = phongChieuDAO.get(idPhong);
			RapPhim rapPhim = phongChieu.getRapPhim();
			for (Integer integer : dsIdChoNgoiLichChieu) {
				ChoNgoiLichChieu choNgoiLichChieu = choNgoiLIchChieuDAO.get(integer);
				if(choNgoiLichChieu.getTrangThai()==1) {
					throw new TicketSellingException();
				}
				choNgoiLichChieus.add(choNgoiLichChieu);
				Ve ve = new Ve();
				ve.setChoNgoiLichChieu(choNgoiLichChieu);
				ve.setRapPhim(rapPhim);
				ve.setThanhTien(choNgoiLichChieu.getLichChieu().getGia());
				ve.setTrangThai(1);
				ves.add(ve);
			}
			Phim phim = choNgoiLichChieus.get(0).getLichChieu().getPhim();
			KhungGio khungGio = choNgoiLichChieus.get(0).getLichChieu().getKhungGio();
			Date ngayChieu = choNgoiLichChieus.get(0).getLichChieu().getNgayChieu();
			request.setAttribute("phim", phim);
			request.setAttribute("khungGio", khungGio);
			request.setAttribute("ves", ves);
			request.setAttribute("rapPhim", rapPhim);
			request.setAttribute("phongChieu", phongChieu);
			request.setAttribute("ngayChieu", ngayChieu);
			session.setAttribute("ves", ves);
			return "GDXacNhanVe";
		} else {
			return null;
		}
	}

	@PostMapping("/findCustomer")
	public String findCustomer(HttpServletRequest request, Model model,
			@RequestParam(value = "maTheThanThiet") String maTheThanThiet) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			HttpSession session = request.getSession();
			model.addAttribute("session", "active");
			KhachHang khachHang = khachHangDAO.getByMaTheThanThiet(maTheThanThiet);
			if (khachHang != null) {
				model.addAttribute("khachHang", khachHang);
				request.setAttribute("khachHang", khachHang);
				session.removeAttribute("khachHang");
				session.setAttribute("khachHang", khachHang);
				return "GDDoiDiem";
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@PostMapping("/checkout")
	public String checkout(HttpServletRequest request, Model model,
			@RequestParam(value = "tongTien") BigDecimal tongTien,
			@RequestParam(value = "diem") Integer diem) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			HttpSession session = request.getSession();
			model.addAttribute("session", "active");
			KhachHang khachHang = (KhachHang) session.getAttribute("khachHang");
			List<Ve> ves = (List<Ve>) session.getAttribute("ves");
			HoaDon hoaDon = new HoaDon();
			for (Ve ve : ves) {

				ChoNgoiLichChieu choNgoiLichChieu = ve.getChoNgoiLichChieu();
				choNgoiLIchChieuDAO.updateTrangThai(choNgoiLichChieu);
			}
			hoaDon.setTongTien(tongTien);
			hoaDon.setVes(ves);
			if (khachHang != null) {
				hoaDon.setKhachHang(khachHang);
				khachHang.setDiem(diem);
				khachHangDAO.updateDiem(khachHang);
			}

			hoaDon.setNguoiBan(nhanVienLogined);
			java.time.LocalDate date = LocalDate.now();
			Date dateSql = Date.valueOf(date);
			hoaDon.setThoiGian(dateSql);
			hoaDonDAO.save(hoaDon);
			for (Ve ve : ves) {
				ve.setHoaDon(hoaDon);
				veDAO.save(ve);
			}
			request.setAttribute("hoaDon", hoaDon);
			session.removeAttribute("khachHang");
			session.removeAttribute("ves");
			return "GDHoaDon";

		}
		return null;
	}
}
