package sonha.com.controller;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sonha.com.dao.ChoNgoiDAO;
import sonha.com.dao.ChoNgoiLichChieuDAO;
import sonha.com.dao.KhachHangDAO;
import sonha.com.dao.KhungGioDAO;
import sonha.com.dao.LichChieuDAO;
import sonha.com.dao.PhimDAO;
import sonha.com.dao.PhongChieuDAO;
import sonha.com.model.ChoNgoi;
import sonha.com.model.ChoNgoiLichChieu;
import sonha.com.model.KhungGio;
import sonha.com.model.LichChieu;
import sonha.com.model.NhanVien;
import sonha.com.model.Phim;
import sonha.com.model.PhongChieu;

/*
 *@Author: sonha
 *@Date: Nov 16, 2021
 *@Version: ver 1.0
 *@Project: PTTKvaĐBCLPM
 */
@Controller
public class LenLichController {
	@Autowired
	private PhimDAO phimDAO;
	@Autowired
	private PhongChieuDAO phongChieuDAO;
	@Autowired
	private LichChieuDAO lichChieuDAO;
	@Autowired
	private ChoNgoiLichChieuDAO choNgoiLIchChieuDAO;
	@Autowired
	private KhachHangDAO khachHangDAO;
	@Autowired
	private KhungGioDAO khungGioDAO;
	@Autowired
	private ChoNgoiDAO choNgoiDAO;

	@GetMapping("/schedule")
	public String showViewLenLich(HttpServletRequest request, Model model) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");
			List<Phim> phims = phimDAO.getAll();
			List<PhongChieu> phongChieus = phongChieuDAO.getAll();
			List<KhungGio> khungGios = khungGioDAO.getAll();
			model.addAttribute("khungGios", khungGios);
			model.addAttribute("phims", phims);
			model.addAttribute("phongChieus", phongChieus);
			return "GDLenLich";
		} else {
			return "GDDangNhap";
		}
	}

	@PostMapping("schedule/save")
	public ResponseEntity<String> createLichChieu(HttpServletRequest request, Model model,
			@RequestParam(value = "idPhong") Integer idPhong, @RequestParam(value = "idPhim") Integer idPhim,
			@RequestParam(value = "idKhungGio") Integer idKhungGio, @RequestParam(value = "gia") BigDecimal gia,
			@RequestParam(value = "ngay") Date ngayChieu) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");
			Phim phim = phimDAO.get(idPhim);
			PhongChieu phongChieu = phongChieuDAO.get(idPhong);
			KhungGio khungGio = khungGioDAO.get(idKhungGio);
			LichChieu lichChieu = new LichChieu();
			lichChieu.setPhim(phim);
			lichChieu.setPhongChieu(phongChieu);
			lichChieu.setGia(gia);
			lichChieu.setKhungGio(khungGio);
			lichChieu.setNgayChieu(ngayChieu);
			if (!lichChieuDAO.save(lichChieu)) {
				return new ResponseEntity<String>("fail", HttpStatus.BAD_REQUEST);
			}
			List<ChoNgoi> choNgois = choNgoiDAO.getByPhongChieu(phongChieu);
			for (ChoNgoi choNgoi : choNgois) {
				ChoNgoiLichChieu choNgoiLichChieu = new ChoNgoiLichChieu();
				choNgoiLichChieu.setChoNgoi(choNgoi);
				choNgoiLichChieu.setLichChieu(lichChieu);
				choNgoiLichChieu.setTrangThai(0);
				choNgoiLIchChieuDAO.save(choNgoiLichChieu);
			}
			return new ResponseEntity<String>("ok", HttpStatus.OK);
		} else {
			return null;
		}
	}
}
