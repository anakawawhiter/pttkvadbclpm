package sonha.com.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sonha.com.dao.ChoNgoiDAO;
import sonha.com.dao.PhongChieuDAO;
import sonha.com.model.ChoNgoi;
import sonha.com.model.NhanVien;
import sonha.com.model.PhongChieu;

/*
 *@Author: sonha
 *@Date: Nov 10, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Controller
public class QLPhongController {
	@Autowired
	private PhongChieuDAO phongChieuDAO;
	@Autowired
	private ChoNgoiDAO choNgoiDAO;

	@GetMapping("/room")
	public String listRoom(HttpServletRequest request, Model model) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");
			List<PhongChieu> phongChieus = phongChieuDAO.getAll();
			model.addAttribute("phongChieus", phongChieus);
			return "GDQuanLyPhong";
		} else {
			return null;
		}
	}

	@PostMapping("/room/add")
	public String addRoom(HttpServletRequest request, Model model,
			@RequestParam(value = "ten") String ten, @RequestParam(value = "soGhe") Integer soGhe) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			PhongChieu phongChieu = new PhongChieu();
			phongChieu.setTen(ten);
			phongChieu.setSoGhe(soGhe);
			List<ChoNgoi> choNgois = new ArrayList<>();
			int rowCount = soGhe / 6;
			for (int row = 1; row <= rowCount; row++) {
				for (int col = 1; col <= 6; col++) {
					ChoNgoi choNgoi = new ChoNgoi();
					choNgoi.setHang(row);
					if (col == 1)
						choNgoi.setCot("A");
					if (col == 2)
						choNgoi.setCot("B");
					if (col == 3)
						choNgoi.setCot("C");
					if (col == 4)
						choNgoi.setCot("D");
					if (col == 5)
						choNgoi.setCot("E");
					if (col == 6)
						choNgoi.setCot("F");
					choNgoi.setPhongChieu(phongChieu);
					choNgois.add(choNgoi);
				}
			}
			phongChieu.setChoNgois(choNgois);
			phongChieuDAO.save(phongChieu);
			for (ChoNgoi choNgoi : choNgois) {
				choNgoiDAO.save(choNgoi);
			}
			model.addAttribute("session", "active");
			List<PhongChieu> phongChieus = phongChieuDAO.getAll();
			model.addAttribute("phongChieus", phongChieus);
			return "GDQuanLyPhong";
		} else {
			return null;
		}
	}
}
