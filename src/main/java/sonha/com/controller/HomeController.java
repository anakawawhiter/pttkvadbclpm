package sonha.com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sonha.com.dao.NhanVienDAO;
import sonha.com.model.NhanVien;

/*
 *@Author: sonha
 *@Date: Nov 6, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Controller
public class HomeController {
	@Autowired
	private NhanVienDAO nhanVienDAO;

	@GetMapping("/home")
	public String hơmePage(HttpServletRequest request, Model model) {
		NhanVien nhanVienLogined = (NhanVien) request.getSession().getAttribute("nhanVienLogined");
		if (nhanVienLogined != null) {
			model.addAttribute("session", "active");
			return "GDChinhNV";
		} else {
			return "GDDangNhap";
		}
	}

	@PostMapping("/login")
	public String login(HttpServletRequest request, Model model,
			@RequestParam("username") String username, @RequestParam("password") String password) {

		NhanVien nhanVienLogined = null;
		System.out.println(username + " " + password);
		nhanVienLogined = nhanVienDAO.login(username, password);
		HttpSession session = request.getSession();
		if (nhanVienLogined != null) {
			session.setAttribute("nhanVienLogined", nhanVienLogined);
			return "GDChinhNV";
		}
		request.getSession().removeAttribute("user");
		return null;
	}

	@GetMapping("/logout")
	public String showLogout(HttpServletRequest request) {
		request.getSession().removeAttribute("nhanVienLogined");
		return "redirect:/home";
	}
}
