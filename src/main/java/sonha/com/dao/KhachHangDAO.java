package sonha.com.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.KhachHang;

/*
 *@Author: sonha
 *@Date: Nov 14, 2021
 *@Version: ver 1.0
 *@Project: PTTKvaĐBCLPM
 */
@Repository
public class KhachHangDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public KhachHang getByMaTheThanThiet(String code) {
		KhachHang khachHang = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(KhachHang.class);
			criteria.add(Restrictions.eq("maTheThanThiet", code));
			khachHang = (KhachHang) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return khachHang;
	}

	public boolean updateDiem(KhachHang t) {
		boolean check = false;
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(t);
			check = true;
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}
}
