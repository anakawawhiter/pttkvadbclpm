package sonha.com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.KhungGio;

/*
 *@Author: sonha
 *@Date: Nov 10, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class KhungGioDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public List<KhungGio> getAll() {
		List<KhungGio> khungGios = new ArrayList<>();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(KhungGio.class);
			khungGios = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return khungGios;
	}

	public KhungGio get(Integer id) {
		KhungGio khungGio = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(KhungGio.class);
			criteria.add(Restrictions.eqOrIsNull("id", id));
			khungGio = (KhungGio) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return khungGio;
	}
}
