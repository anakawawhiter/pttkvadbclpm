package sonha.com.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.NhanVien;

/*
 *@Author: sonha
 *@Date: Nov 7, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class NhanVienDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public NhanVien login(String taiKhoan, String matKhau) {
		NhanVien nhanVien = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(NhanVien.class);
			criteria.add(Restrictions.eq("matKhau", matKhau));
			criteria.add(Restrictions.eq("taiKhoan", taiKhoan));
			nhanVien = (NhanVien) criteria.uniqueResult();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return nhanVien;
	}

}
