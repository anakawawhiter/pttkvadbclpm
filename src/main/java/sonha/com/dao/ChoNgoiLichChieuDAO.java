package sonha.com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.ChoNgoiLichChieu;
import sonha.com.model.LichChieu;

/*
 *@Author: sonha
 *@Date: Nov 7, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class ChoNgoiLichChieuDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public ChoNgoiLichChieu get(Integer id) {
		ChoNgoiLichChieu choNgoiLichChieu = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(ChoNgoiLichChieu.class);
			criteria.add(Restrictions.eq("id", id));
			choNgoiLichChieu = (ChoNgoiLichChieu) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return choNgoiLichChieu;
	}

	public List<ChoNgoiLichChieu> getAllByLichChieu(LichChieu lichChieu) {
		List<ChoNgoiLichChieu> choNgoiLichChieus = new ArrayList<ChoNgoiLichChieu>();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(ChoNgoiLichChieu.class);
			criteria.add(Restrictions.eq("lichChieu", lichChieu));
			choNgoiLichChieus = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return choNgoiLichChieus;
	}

	public boolean updateTrangThai(ChoNgoiLichChieu choNgoiLichChieu) {
		boolean check = false;
		Session session = null;
		Transaction tx = null;
		try {
			choNgoiLichChieu.setTrangThai(1);
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(choNgoiLichChieu);
			tx.commit();
			check = true;
		} catch (Exception e) {
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}

	public boolean save(ChoNgoiLichChieu t) {
		boolean check = false;
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(t);
			tx.commit();
			check = true;

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}
}
