package sonha.com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.Ve;

/*
 *@Author: sonha
 *@Date: Nov 11, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class VeDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public boolean save(Ve t) {
		boolean check = false;
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(t);
			tx.commit();
			check = true;

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}

}
