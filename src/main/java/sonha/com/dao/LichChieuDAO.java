package sonha.com.dao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.LichChieu;
import sonha.com.model.Phim;
import sonha.com.model.PhongChieu;

/*
 *@Author: sonha
 *@Date: Nov 7, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class LichChieuDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public LichChieu get(Integer id) {
		LichChieu lichChieu = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(LichChieu.class);
			criteria.add(Restrictions.eq("id", id));
			lichChieu = (LichChieu) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return lichChieu;
	}

	public List<LichChieu> getLichChieuByPhim(int position, int pageSize, Phim phim) {
		List<LichChieu> lichChieus = new ArrayList<LichChieu>();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(LichChieu.class);
			criteria.add(Restrictions.eq("phim", phim));
			java.time.LocalDate date = LocalDate.now();
			Date dateSql = Date.valueOf(date);
			System.out.println(dateSql);
			criteria.addOrder(Order.asc("ngayChieu"));
			criteria.add(Restrictions.gt("ngayChieu", dateSql));
			criteria.setFirstResult(position);
			criteria.setMaxResults(pageSize);
			lichChieus = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return lichChieus;
	}

	public List<LichChieu> getLichChieuByPhongChieu(int position, int pageSize, PhongChieu phongChieu) {
		List<LichChieu> lichChieus = new ArrayList<LichChieu>();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(LichChieu.class);
			criteria.add(Restrictions.eq("phongChieu", phongChieu));
			java.time.LocalDate date = LocalDate.now();
			Date dateSql = Date.valueOf(date);
			System.out.println(dateSql);
			criteria.addOrder(Order.asc("ngayChieu"));
			criteria.add(Restrictions.gt("ngayChieu", dateSql));
			criteria.setFirstResult(position);
			criteria.setMaxResults(pageSize);
			lichChieus = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return lichChieus;
	}

	public int totalRecordByPhim(Phim phim) {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(LichChieu.class);
		java.time.LocalDate date = LocalDate.now();
		Date dateSql = Date.valueOf(date);
		System.out.println(dateSql);
		criteria.add(Restrictions.gt("ngayChieu", dateSql));
		criteria.add(Restrictions.eq("phim", phim));
		int record = (int) criteria.list().size();
		session.close();
		return record;
	}

	public int totalRecordByPhong(PhongChieu phongChieu) {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(LichChieu.class);
		java.time.LocalDate date = LocalDate.now();
		Date dateSql = Date.valueOf(date);
		System.out.println(dateSql);
		criteria.add(Restrictions.gt("ngayChieu", dateSql));
		criteria.add(Restrictions.eq("phongChieu", phongChieu));
		int record = (int) criteria.list().size();
		session.close();
		return record;
	}

	public boolean save(LichChieu t) {
		boolean check = false;
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(t);
			tx.commit();
			check = true;

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}
}
