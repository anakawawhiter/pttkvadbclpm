package sonha.com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.ChoNgoi;
import sonha.com.model.PhongChieu;

/*
 *@Author: sonha
 *@Date: Nov 10, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class ChoNgoiDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public boolean save(ChoNgoi t) {
		boolean check = false;
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(t);
			check = true;
			tx.commit();

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}

	public List<ChoNgoi> getByPhongChieu(PhongChieu phongChieu) {
		List<ChoNgoi> choNgois = new ArrayList<>();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(ChoNgoi.class);
			criteria.add(Restrictions.eq("phongChieu", phongChieu));
			choNgois = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return choNgois;
	}

}
