package sonha.com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.HoaDon;

/*
 *@Author: sonha
 *@Date: Nov 15, 2021
 *@Version: ver 1.0
 *@Project: PTTKvaĐBCLPM
 */
@Repository
public class HoaDonDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public boolean save(HoaDon hoaDon) {
		boolean check = false;
		Session session = null;
		Transaction transaction = null;
		try {
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.save(hoaDon);
			transaction.commit();
			check = true;
		} catch (Exception e) {
			transaction.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}
}
