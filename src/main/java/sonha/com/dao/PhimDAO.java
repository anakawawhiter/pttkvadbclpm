package sonha.com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.Phim;

/*
 *@Author: sonha
 *@Date: Nov 7, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class PhimDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public Phim get(Integer id) {
		Phim phim = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(Phim.class);
			criteria.add(Restrictions.eq("id", id));
			phim = (Phim) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return phim;
	}

	public List<Phim> getAll() {
		List<Phim> phims = new ArrayList<Phim>();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(Phim.class);
			phims = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return phims;
	}

}
