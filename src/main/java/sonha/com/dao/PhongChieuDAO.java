package sonha.com.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sonha.com.model.LichChieu;
import sonha.com.model.PhongChieu;

/*
 *@Author: sonha
 *@Date: Nov 7, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Repository
public class PhongChieuDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public PhongChieu get(Integer id) {
		PhongChieu phongChieu = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(PhongChieu.class);
			criteria.add(Restrictions.eq("id", id));
			phongChieu = (PhongChieu) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return phongChieu;
	}

	public List<PhongChieu> getAll() {
		List<PhongChieu> phongChieus = new ArrayList<PhongChieu>();
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(PhongChieu.class);
			phongChieus = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return phongChieus;
	}

	public boolean save(PhongChieu t) {
		boolean check = false;
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(t);
			tx.commit();
			check = true;

		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return check;
	}

	public PhongChieu getByLichChieu(LichChieu lichChieu) {
		PhongChieu phongChieu = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Criteria criteria = session.createCriteria(PhongChieu.class);
			criteria.add(Restrictions.eq("lichChieu", lichChieu));
			phongChieu = (PhongChieu) criteria.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return phongChieu;
	}

}
