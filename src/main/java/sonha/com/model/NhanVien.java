package sonha.com.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 2, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "nhan_vien")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NhanVien implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "ten", nullable = false)
	private String ten;
	@Column(name = "dai_chi", nullable = false)
	private String diaChi;
	@Column(name = "so_dien_thoai", nullable = false)
	private String soDienThoai;
	@Column(name = "cmt", nullable = false, unique = true)
	private String cmt;
	@Column(name = "chuc_vu", nullable = false)
	private String chucVu;
	@Column(name = "ngay_sinh", nullable = false)
	private Date ngaySinh;
	@Column(name = "tai_khoan", nullable = false, unique = true)
	private String taiKhoan;
	@Column(name = "mat_khau", nullable = false)
	private String matKhau;
}
