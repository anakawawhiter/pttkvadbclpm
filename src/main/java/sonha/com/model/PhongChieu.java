package sonha.com.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 2, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "phong_chieu")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PhongChieu implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "ten", nullable = false, unique = true)
	private String ten;
	@Column(name = "so_ghe", nullable = false)
	private Integer soGhe;
	@OneToMany(mappedBy = "phongChieu")
	private List<ChoNgoi> choNgois;
	@OneToMany(mappedBy = "phongChieu", fetch = FetchType.LAZY)
	private List<LichChieu> lichChieus;
	@ManyToOne
	@JoinColumn(name = "id_rap_phim")
	private RapPhim rapPhim;
}
