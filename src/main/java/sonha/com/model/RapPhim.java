package sonha.com.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "rap_phim")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RapPhim implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "ten", nullable = false, unique = true)
	private String ten;
	@Column(name = "dia_chi", nullable = false)
	private String diaChi;
	@Column(name = "hot_line", nullable = false)
	private String hotLine;
}
