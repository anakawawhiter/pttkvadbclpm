package sonha.com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 6, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "hoa_don_dich_vu")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HoaDonDichVu implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@ManyToOne
	@JoinColumn(name = "id_hoa_don")
	private HoaDon hoaDon;
	@ManyToOne
	@JoinColumn(name = "id_dich_vu")
	private DichVu dichVu;
	@Column(name = "so_luong", nullable = false)
	private Integer soLuong;
}
