package sonha.com.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 3, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "lich_chieu",uniqueConstraints = @UniqueConstraint(columnNames = {"id_phong_chieu","id_phim","id_khung_gio","ngay_chieu"}))
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LichChieu implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "gia", nullable = false)
	private BigDecimal gia;
	@Column(name = "ngay_chieu")
	@ColumnDefault( value = "CURRENT_TIMESTAMP" )
	private Date ngayChieu;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_phong_chieu")
	private PhongChieu phongChieu;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_phim")
	private Phim phim;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_khung_gio")
	private KhungGio khungGio;
}
