package sonha.com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 2, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "cho_ngoi_lich_chieu")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChoNgoiLichChieu implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "trang_thai", nullable = false, length = 2)
	private Integer trangThai;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_cho_ngoi")
	private ChoNgoi choNgoi;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_lich_chieu")
	private LichChieu lichChieu;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_ve")
	private Ve ve;

	@Override
	public String toString() {
		return "ChoNgoiLichChieu [id=" + id + ", trangThai=" + trangThai + ", choNgoi=" + choNgoi
				+ ", lichChieu=" + lichChieu + ", ve=" + ve + "]";
	}

}
