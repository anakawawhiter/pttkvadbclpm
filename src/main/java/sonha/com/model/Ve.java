package sonha.com.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 3, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "ve")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ve implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "thanh_tien", nullable = false)
	private BigDecimal thanhTien;
	@Column(name = "trang_thai", nullable = false)
	private Integer trangThai;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_cho_ngoi_lich_chieu")
	private ChoNgoiLichChieu choNgoiLichChieu;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_rap_phim")
	private RapPhim rapPhim;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_hoa_don")
	private HoaDon hoaDon;

	@Override
	public String toString() {
		return "Ve [id=" + id + ", thanhTien=" + thanhTien + ", choNgoiLichChieu=" + choNgoiLichChieu
				+ ", rapPhim=" + rapPhim + ", hoaDon=" + hoaDon + "]";
	}

}
