package sonha.com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 2, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "khach_hang")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KhachHang implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "ten", nullable = false)
	private String ten;
	@Column(name = "dai_chi", nullable = false)
	private String diaChi;
	@Column(name = "so_dien_thoai", nullable = false)
	private String soDienThoai;
	@Column(name = "ma_the_than_thiet", nullable = false)
	private String maTheThanThiet;
	@Column(name = "diem", nullable = false)
	private Integer diem;

	@Override
	public String toString() {
		return "KhachHang [id=" + id + ", ten=" + ten + ", diaChi=" + diaChi + ", soDienThoai="
				+ soDienThoai + ", maTheThanThiet=" + maTheThanThiet + ", diem=" + diem + "]";
	}

}
