package sonha.com.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 3, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "hoa_don")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HoaDon implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "thoi_gian", nullable = false)
	private Date thoiGian;
	@Column(name = "tong_tien", nullable = false)
	private BigDecimal tongTien;
	@OneToMany(mappedBy = "hoaDon")
	private List<Ve> ves;
	@OneToMany(mappedBy = "hoaDon")
	private List<HoaDonDichVu> hoaDonDichVus;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_nhan_vien")
	private NhanVien nguoiBan;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_khach_hang")
	private KhachHang khachHang;

	@Override
	public String toString() {
		return "HoaDon [id=" + id + ", thoiGian=" + thoiGian + ", tongTien=" + tongTien + ", nguoiBan="
				+ nguoiBan + ", khachHang=" + khachHang + "]";
	}

}
