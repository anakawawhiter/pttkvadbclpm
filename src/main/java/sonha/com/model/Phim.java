package sonha.com.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*
 *@Author: sonha
 *@Date: Nov 2, 2021
 *@Version: ver 1.0
 *@Project: Cinema
 */
@Entity
@Table(name = "phim")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Phim implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "ten", nullable = false)
	private String ten;
	@Column(name = "the_loai", nullable = false)
	private String theLoai;
	@Column(name = "thoi_luong", nullable = false)
	private Integer thoiLuong;
	@OneToMany(mappedBy = "phim", fetch = FetchType.LAZY)
	private List<LichChieu> lichChieus;
}
