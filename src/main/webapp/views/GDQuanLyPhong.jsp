<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="text-center mt-3 ">
	<h5 class="card-header text-center">Quản lý phòng chiếu</h5>
	<!--<div class="alert alert-danger " id="success-alert">
		<strong class="h3">Room name is exist!</strong>
	</div>-->
</div>
<hr>
<div class="card-body">
	<div class="d-flex justify-content-between mb-3">
		<a class="btn btn-primary btn-blue" href="#" id="addNewBtn"><i
			class="fas fa-plus-circle"></i>&nbsp;Add new</a>
		<form>
			<div class="row">
				<div class="col">
					<select class="custom-select" id="searchType">
						<option value="">Filter by</option>

						<option value="roomName">Tên phòng</option>



						<option value="seatQuantity">Sức chứa</option>

					</select>
				</div>
				<div class="col">
					<input type="text" class="form-control" id="searchText"
						value="${searchText}" maxlength="28">
				</div>
				<button type="button" class="btn btn-primary btn-blue mr-3"
					id="search-room">
					<i class="fas fa-search"></i>
				</button>
			</div>
		</form>
	</div>
	<br>
	<div class="row">
		<div class="table-responsive">
			<table class="table table-striped">
				<thead class="">
					<th>#</th>
					<th>ID phòng</th>
					<th>Tên</th>
					<th>Sức chứa</th>
					<th>Chi tiết</th>
				</thead>
				<tbody>
					<c:forEach items="${phongChieus}" var="phong" varStatus="index">
						<tr>
							<td>${index.index+1}</td>
							<td>${phong.id}</td>
							<td>${phong.ten}</td>
							<td>${phong.soGhe}</td>
							<td><a href="#" th:id="${phong.id}"
								class="text-primary seat-detail">Xoa</a></td>
						</tr>
					</c:forEach>
				</tbody>
				<tfoot>
				</tfoot>
			</table>
		</div>
		<br>
	</div>

	<script src='<c:url value="/resources/js/add-room.js" />'></script>
</div>
<!-- The Modal add-->
<div class="modal" id="modalAddRoom">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="/addRoom">
				<!-- Modal Header -->
				<div
					class="modal-header text-center align-items-center justify-content-center">
					<h4 class="modal-title ">Add new room</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<div class="">
						<label for="cinemaRoom" class="form-text">Cinema room<span
							class="text-danger">*</span></label> <input type="text"
							class="form-control" id="cinemaRoom" name="cinemaRoom" required
							placeholder="enter cinema room name"> <span
							id="messCinemaRoom"></span>
					</div>
					<div class="">
						<label for="seatQuantity" class="form-text">Seat quantity<span
							class="text-danger">*</span>
						</label> <input type="number" class="form-control" id="seatQuantity"
							name="seatQuantity" required min="0" step="6"> <span
							id="messSeatQuantity"></span>
					</div>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer justify-content-between">
					<button type="reset" class="btn btn-success">Reset</button>
					<button type="button" id="add-room" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>