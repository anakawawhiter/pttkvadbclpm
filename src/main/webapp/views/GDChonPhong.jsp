<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<select class="btn btn-outline-dark" name="" id="selected-room">
	<c:forEach items="${phongChieus}" var="phong" varStatus="index">
		<option value="${phong.id}" id="${phong.id}">${phong.ten}</option>
	</c:forEach>

</select>
<button id="showtime-by-room" class="btn btn-primary">Xem các
	lịch chiếu</button>
<script src='<c:url value="/resources/js/showTime.js" />'></script>

