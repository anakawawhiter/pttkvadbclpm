<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<select class="btn btn-outline-dark" name="" id="selected-film">
	<c:forEach items="${phims}" var="phim" varStatus="index">
		<option value="${phim.id}" id="${phim.id}">${phim.ten}</option>
	</c:forEach>

</select>
<button id="showtime-by-film" class="btn btn-primary">Xem các
	lịch chiếu</button>
<script src='<c:url value="/resources/js/showTime.js" />'></script>

