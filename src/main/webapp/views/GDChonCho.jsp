<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="content container-fluid">
	<div class="text-center mt-2 h2">
		<Span class="room" id="${phongChieu.id}">Chọn chỗ phòng :
			${phongChieu.ten}</Span>
	</div>
	<hr>
	<br>
	<div class="row">
		<div
			class="seat-container col-xl-8 col-12 col-sm-12 col-lg-10 col-md-10 offset-md-1  offset-lg-1  offset-xl-3">
			<div class="row"><span class="h2">Màn hình  <i class="fa fa-arrow-up" aria-hidden="true"></i></span></div>
			<div class="row">
				<c:forEach items="${choNgoiLichChieus}" var="cho" varStatus="index">
					<c:if test="${cho.trangThai == 0}">
						<button type="button" id="${cho.id}"
							class="seat  btn btn-sm btn-lg  btn-lx btn-md btn-light col-1 btn-outline-dark m-2">${cho.choNgoi.hang}${cho.choNgoi.cot}</button>
					</c:if>
					<c:if test="${cho.trangThai == 1}">
						<button type="button" id="${cho.id}"
							class="seat  btn btn-sm btn-lg  btn-lx btn-md bg-danger disabled col-1 btn-outline-dark m-2"
							disabled>${cho.choNgoi.hang}${cho.choNgoi.cot}</button>
					</c:if>
					<c:if
						test="${((index.index + 1) % 3 == 0) && ((index.index + 1) %6 !=0)}">
						<div class="col-1"></div>
					</c:if>
					<c:if test="${(index.index +1 )% 6 == 0}">
			</div>
			<div class="row">
				</c:if>
				</c:forEach>
			</div>
			<br> <br>
		</div>
	</div>
	<hr>
	<div class="text-center">
		<span><Span class="text-success"><i class="fa fa-square"
				aria-hidden="true"></i></Span>&nbsp; Đang chọn</span> <span><Span
			class="text-danger"><i class="fa fa-square" aria-hidden="true"></i></Span>&nbsp;
			Đã bán</span> <span><Span class=""><i class="far fa-square"></i></Span>&nbsp;
			Có thể chọn</span>
	</div>
	<br>
	<div class="text-center">
		<button class="btn btn-primary" id="book">
			<i class="fa fa-check" aria-hidden="true"></i>Tiếp
		</button>
		<button class="btn btn-primary" id="back">
			<i class="fa fa-times" aria-hidden="true"></i>Về
		</button>
	</div>
	<script src='<c:url value="/resources/js/selectSeat.js" />'></script>
</div>