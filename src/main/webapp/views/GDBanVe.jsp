<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="list-film col-10 col-lg-10 col-md-10 col-sm-12">
	<div class="row" id="">
		<div class="col-12 justify-content-between">
			<button
				class="btn btn-outline-dark btn-primamy active option-booking"
				id="select-by-film">Chọn theo phim</button>
			<button class="btn btn-outline-dark btn-primamy option-booking"
				id="select-by-room">Chọn theo phòng</button>
		</div>
		<div id="seclect-box" class="col-12 justify-content-between m-2">
			<select class="btn btn-outline-dark" name="" id="selected-film">
				<c:forEach items="${phims}" var="phim" varStatus="index">
					<option value="${phim.id}" id="${phim.id}">${phim.ten}</option>
				</c:forEach>

			</select>
			<button id="showtime-by-film" class="btn btn-primary">Xem
				các lịch chiếu</button>
			<script src='<c:url value="/resources/js/showTime.js" />'></script>
		</div>
	</div>
	<br>
	<hr>
	<div id="time-show">
		<c:forEach items="${lichChieus}" var="lich" varStatus="index">
			<div class="row ">
				<a href="#" class="item d-flex">
					<div>
						<img src='
			<c:url value="/resources/img/imgcard.png" />
			'
							style="max-width: 100px;" alt="phim">
					</div>
					<div>
						<div>
							<span>Tên phim: ${lich.phim.ten}</span>
						</div>
						<div>
							<Span>Thời lượng: ${lich.phim.thoiLuong}</Span>
						</div>
						<div>
							<span>Giò chiếu: ${lich.khungGio.ten}</span>
						</div>
						<div>
							<span>Phòng chiếu: ${lich.phongChieu.ten}</span>
						</div>
						<div>
							<span>Giá vé: ${lich.gia}</span>
						</div>
					</div>
				</a>
			</div>
		</c:forEach>
	</div>
</div>
<script src='<c:url value="/resources/js/selling.js" />'></script>
</div>