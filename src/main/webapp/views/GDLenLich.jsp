<%--
  Created by IntelliJ IDEA.
  User: sonha
  Date: 20-Nov-21
  Time: 5:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="list-film col-10 col-lg-10 col-md-10 col-sm-12">
	<div class="h3  text-center d-flex justify-content-center">Lên lịch</div>
	<div class="row">
		<div class="col-8">
			<form action="" class="form">
				<div class="form-group">
					<label for="selected-film">Chọn phim</label> <select
						class="form-control" name="" id="selected-film">
						<c:forEach items="${phims}" var="phim" varStatus="index">
							<option value="${phim.id}" id="${phim.id}">${phim.ten}</option>
						</c:forEach>
					</select>
				</div>

				<div class="form-group">
					<label for="selected-film">Chọn phòng</label> <select
						class="form-control" name="" id="selected-room">
						<c:forEach items="${phongChieus}" var="phong" varStatus="index">
							<option value="${phong.id}" id="${phong.id}">${phong.ten}</option>
						</c:forEach>
					</select>
				</div>

				<div class="form-group">
					<label for="selected-film">Chọn khung giờ</label> <select
						class="form-control" name="" id="selected-hour">
						<c:forEach items="${khungGios}" var="gio" varStatus="index">
							<option value="${gio.id}" id="${gio.id}">${gio.ten}</option>
						</c:forEach>
					</select>
				</div>

				<div class="form-group">
					<label for="selected-film">Chọn giá vé</label> <select
						class="form-control" name="" id="selected-price">
						<option value="55000">55000 vnđ</option>
						<option value="85000">85000 vnđ</option>
						<option value="100000">100000 vnđ</option>
						<option value="120000">120000 vnđ</option>
					</select>
				</div>

				<div class="form-group">
					<label for="selected-date">Chọn ngày</label> <input type="date"
						class="form-control" id="selected-date" name="selected-date" required="required">
						<div id="validate"></div>
				</div>
				<button type="button" class="btn btn-primary" id="btn-schedule" >Lên
					lịch</button>
				<div id="mess"></div>
			</form>
		</div>
	</div>
	<script src='<c:url value="/resources/js/schedule.js" />'></script>
</div>