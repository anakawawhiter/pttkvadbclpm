$(document).ready(function() {
    $(".option-booking").click(function(e) {
        $(".option-booking").removeClass("active");
        $(this).addClass("active");
    });
    $("#select-by-film").click(function(e) {
        $.get({
            url: "./selectByFilm",
            success: function(response) {
                $("#seclect-box").html(response);
            }
        });
    });
    $("#select-by-room").click(function(e) {
        $.get({
            url: "./selectByRoom",
            success: function(response) {
                $("#seclect-box").html(response);
            }
        });
    });
});