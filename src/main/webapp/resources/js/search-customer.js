$(document).ready(function() {

    $("#btn-search-cus").click(function(e) {
        var cusCode = $("#customer-code").val();
        console.log(cusCode);
        var eleList = $(".money");
        var sum = 0;
        $.map(eleList, function(elementOrValue, indexOrKey) {
            sum += parseInt(elementOrValue.innerText) + 0;
        });
        console.log(sum);
        $("#total").html(sum);
        $.post({
            url: "./findCustomer",
            data: {
                maTheThanThiet: cusCode
            },
            success: function(response) {
                $("#score-exchange").html(response);
                $("#messErr").text("");
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $("#messErr").text("Không tim thấy khách hàng vui lòng kiểm tra lại");
            }
        });
    });
});