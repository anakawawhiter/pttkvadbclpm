$(document).ready(function() {
	$("#btn-schedule").click(function(e) {
		var idFilm = $("#selected-film").val() * 1;
		var idRoom = $("#selected-room").val() * 1;
		var idHour = $("#selected-hour").val() * 1;
		var price = $("#selected-price").val() * 1;
		var date = $("#selected-date").val();
		var today = new Date();
		var choosedDate = new Date(date);
		console.log(idFilm + " " + idRoom + " " + idHour + " " + date + " " + price);
		if (date == "") {
			$("#validate").html('<span class="text-danger">Bạn chưa chọn ngày chiếu!</span>');
		} else if (choosedDate < today) {
			$("#validate").html('<span class="text-danger">Ngày chiếu phải ít nhất sau hôm nay!</span>');
		} else {
			$("#validate").html('');
			$("#mess").html('<span class="h4 text-success">Chờ chút...</span>');
			$.post({
				url: "./schedule/save",
				data: {
					idPhong: idRoom,
					idPhim: idFilm,
					idKhungGio: idHour,
					gia: price,
					ngay: date
				},
				success: function(response) {
					$("#mess").html('<span class="h4 text-success">Lên lịch thành công</span>');
				},
				error: function() {
					$("#mess").html('<span class="h4 text-danger">Lên lịch thất bại</span>');
				}
			});
		}

	});

});