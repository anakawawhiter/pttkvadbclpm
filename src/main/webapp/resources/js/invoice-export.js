$(document).ready(function() {
    $("#checkout").click(function(e) {
        var total = parseInt($("#total").text());
        var newpoint = parseInt($("#surplus").text());
        var diem;
        if (isNaN(newpoint)) {
            diem = 0;
        } else {
            diem = newpoint;
        }
        console.log(newpoint);
        $.post({
            url: "./checkout",
            data: {
                tongTien: total,
                diem: diem
            },
            success: function(response) {
                $("#content").html(response);
            }
        });
    });
});