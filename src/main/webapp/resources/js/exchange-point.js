$(document).ready(function() {
    $("#exchange-btn").click(function(e) {
        var point = parseInt($("#point").text()) * 1000;
        var ticketprice = parseInt($(".money").text());
        var total = parseInt($("#total").text());
        var amount = parseInt($("#amount").val());
        console.log(point);
        console.log(ticketprice);
        console.log(total);
        console.log(amount);
        if (point < ticketprice) {
            $("#notify").text("Chà chà, điểm chưa đủ đổi rồi!");
        } else {
            while (amount > 0 && point > ticketprice) {
                total -= ticketprice;
                point -= ticketprice;
                amount--;
            }
            $("#surplus").text(point / 1000);
            $("#total").text(total);
            $("#exchange-btn").attr("disabled", true);
        }
    });
});