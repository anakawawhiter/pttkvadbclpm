USE [cinema]
GO
SET IDENTITY_INSERT [dbo].[rap_phim] ON 
GO
INSERT [dbo].[rap_phim] ([id], [dia_chi], [hot_line], [ten]) VALUES (1, N'HN', N'073903277', N'SohaPhim')
GO
INSERT [dbo].[rap_phim] ([id], [dia_chi], [hot_line], [ten]) VALUES (2, N'HN', N'387638768', N'phimplus')
GO
INSERT [dbo].[rap_phim] ([id], [dia_chi], [hot_line], [ten]) VALUES (3, N'HN', N'231231232', N'phimmoi')
GO
SET IDENTITY_INSERT [dbo].[rap_phim] OFF
GO
SET IDENTITY_INSERT [dbo].[phong_chieu] ON 
GO
INSERT [dbo].[phong_chieu] ([id], [so_ghe], [ten], [id_rap_phim]) VALUES (1, 60, N'Phòng 101', 1)
GO
INSERT [dbo].[phong_chieu] ([id], [so_ghe], [ten], [id_rap_phim]) VALUES (2, 66, N'Phòng 102', 1)
GO
INSERT [dbo].[phong_chieu] ([id], [so_ghe], [ten], [id_rap_phim]) VALUES (3, 48, N'Phòng 103', 1)
GO
INSERT [dbo].[phong_chieu] ([id], [so_ghe], [ten], [id_rap_phim]) VALUES (4, 60, N'Phòng 104', 1)
GO
INSERT [dbo].[phong_chieu] ([id], [so_ghe], [ten], [id_rap_phim]) VALUES (5, 66, N'Phòng 201', 1)
GO
INSERT [dbo].[phong_chieu] ([id], [so_ghe], [ten], [id_rap_phim]) VALUES (6, 66, N'Phòng 202', 1)
GO
SET IDENTITY_INSERT [dbo].[phong_chieu] OFF
GO
SET IDENTITY_INSERT [dbo].[cho_ngoi] ON 
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (1, N'A', 1, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (2, N'B', 1, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (3, N'C', 1, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (4, N'D', 1, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (5, N'E', 1, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (6, N'F', 1, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (7, N'A', 2, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (8, N'B', 2, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (9, N'C', 2, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (10, N'D', 2, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (11, N'E', 2, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (12, N'F', 2, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (13, N'A', 3, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (14, N'B', 3, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (15, N'C', 3, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (16, N'D', 3, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (17, N'E', 3, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (18, N'F', 3, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (19, N'A', 4, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (20, N'B', 4, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (21, N'C', 4, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (22, N'D', 4, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (23, N'E', 4, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (24, N'F', 4, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (25, N'A', 5, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (26, N'B', 5, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (27, N'C', 5, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (28, N'D', 5, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (29, N'E', 5, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (30, N'F', 5, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (31, N'A', 6, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (32, N'B', 6, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (33, N'C', 6, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (34, N'D', 6, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (35, N'E', 6, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (36, N'F', 6, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (37, N'A', 7, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (38, N'B', 7, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (39, N'C', 7, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (40, N'D', 7, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (41, N'E', 7, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (42, N'F', 7, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (43, N'A', 8, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (44, N'B', 8, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (45, N'C', 8, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (46, N'D', 8, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (47, N'E', 8, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (48, N'F', 8, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (49, N'A', 9, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (50, N'B', 9, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (51, N'C', 9, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (52, N'D', 9, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (53, N'E', 9, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (54, N'F', 9, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (55, N'A', 10, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (56, N'B', 10, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (57, N'C', 10, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (58, N'D', 10, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (59, N'E', 10, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (60, N'F', 10, 1)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (61, N'A', 1, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (62, N'B', 1, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (63, N'C', 1, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (64, N'D', 1, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (65, N'E', 1, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (66, N'F', 1, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (67, N'A', 2, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (68, N'B', 2, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (69, N'C', 2, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (70, N'D', 2, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (71, N'E', 2, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (72, N'F', 2, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (73, N'A', 3, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (74, N'B', 3, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (75, N'C', 3, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (76, N'D', 3, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (77, N'E', 3, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (78, N'F', 3, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (79, N'A', 4, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (80, N'B', 4, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (81, N'C', 4, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (82, N'D', 4, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (83, N'E', 4, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (84, N'F', 4, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (85, N'A', 5, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (86, N'B', 5, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (87, N'C', 5, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (88, N'D', 5, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (89, N'E', 5, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (90, N'F', 5, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (91, N'A', 6, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (92, N'B', 6, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (93, N'C', 6, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (94, N'D', 6, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (95, N'E', 6, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (96, N'F', 6, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (97, N'A', 7, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (98, N'B', 7, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (99, N'C', 7, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (100, N'D', 7, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (101, N'E', 7, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (102, N'F', 7, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (103, N'A', 8, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (104, N'B', 8, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (105, N'C', 8, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (106, N'D', 8, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (107, N'E', 8, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (108, N'F', 8, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (109, N'A', 9, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (110, N'B', 9, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (111, N'C', 9, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (112, N'D', 9, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (113, N'E', 9, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (114, N'F', 9, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (115, N'A', 10, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (116, N'B', 10, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (117, N'C', 10, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (118, N'D', 10, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (119, N'E', 10, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (120, N'F', 10, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (121, N'A', 11, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (122, N'B', 11, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (123, N'C', 11, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (124, N'D', 11, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (125, N'E', 11, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (126, N'F', 11, 2)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (127, N'A', 1, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (128, N'B', 1, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (129, N'C', 1, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (130, N'D', 1, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (131, N'E', 1, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (132, N'F', 1, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (133, N'A', 2, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (134, N'B', 2, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (135, N'C', 2, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (136, N'D', 2, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (137, N'E', 2, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (138, N'F', 2, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (139, N'A', 3, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (140, N'B', 3, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (141, N'C', 3, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (142, N'D', 3, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (143, N'E', 3, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (144, N'F', 3, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (145, N'A', 4, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (146, N'B', 4, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (147, N'C', 4, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (148, N'D', 4, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (149, N'E', 4, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (150, N'F', 4, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (151, N'A', 5, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (152, N'B', 5, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (153, N'C', 5, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (154, N'D', 5, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (155, N'E', 5, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (156, N'F', 5, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (157, N'A', 6, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (158, N'B', 6, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (159, N'C', 6, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (160, N'D', 6, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (161, N'E', 6, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (162, N'F', 6, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (163, N'A', 7, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (164, N'B', 7, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (165, N'C', 7, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (166, N'D', 7, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (167, N'E', 7, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (168, N'F', 7, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (169, N'A', 8, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (170, N'B', 8, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (171, N'C', 8, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (172, N'D', 8, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (173, N'E', 8, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (174, N'F', 8, 3)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (175, N'A', 1, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (176, N'B', 1, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (177, N'C', 1, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (178, N'D', 1, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (179, N'E', 1, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (180, N'F', 1, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (181, N'A', 2, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (182, N'B', 2, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (183, N'C', 2, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (184, N'D', 2, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (185, N'E', 2, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (186, N'F', 2, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (187, N'A', 3, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (188, N'B', 3, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (189, N'C', 3, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (190, N'D', 3, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (191, N'E', 3, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (192, N'F', 3, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (193, N'A', 4, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (194, N'B', 4, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (195, N'C', 4, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (196, N'D', 4, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (197, N'E', 4, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (198, N'F', 4, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (199, N'A', 5, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (200, N'B', 5, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (201, N'C', 5, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (202, N'D', 5, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (203, N'E', 5, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (204, N'F', 5, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (205, N'A', 6, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (206, N'B', 6, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (207, N'C', 6, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (208, N'D', 6, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (209, N'E', 6, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (210, N'F', 6, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (211, N'A', 7, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (212, N'B', 7, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (213, N'C', 7, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (214, N'D', 7, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (215, N'E', 7, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (216, N'F', 7, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (217, N'A', 8, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (218, N'B', 8, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (219, N'C', 8, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (220, N'D', 8, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (221, N'E', 8, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (222, N'F', 8, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (223, N'A', 9, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (224, N'B', 9, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (225, N'C', 9, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (226, N'D', 9, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (227, N'E', 9, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (228, N'F', 9, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (229, N'A', 10, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (230, N'B', 10, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (231, N'C', 10, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (232, N'D', 10, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (233, N'E', 10, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (234, N'F', 10, 4)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (235, N'A', 1, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (236, N'B', 1, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (237, N'C', 1, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (238, N'D', 1, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (239, N'E', 1, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (240, N'F', 1, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (241, N'A', 2, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (242, N'B', 2, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (243, N'C', 2, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (244, N'D', 2, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (245, N'E', 2, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (246, N'F', 2, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (247, N'A', 3, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (248, N'B', 3, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (249, N'C', 3, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (250, N'D', 3, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (251, N'E', 3, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (252, N'F', 3, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (253, N'A', 4, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (254, N'B', 4, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (255, N'C', 4, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (256, N'D', 4, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (257, N'E', 4, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (258, N'F', 4, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (259, N'A', 5, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (260, N'B', 5, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (261, N'C', 5, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (262, N'D', 5, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (263, N'E', 5, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (264, N'F', 5, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (265, N'A', 6, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (266, N'B', 6, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (267, N'C', 6, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (268, N'D', 6, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (269, N'E', 6, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (270, N'F', 6, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (271, N'A', 7, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (272, N'B', 7, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (273, N'C', 7, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (274, N'D', 7, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (275, N'E', 7, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (276, N'F', 7, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (277, N'A', 8, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (278, N'B', 8, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (279, N'C', 8, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (280, N'D', 8, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (281, N'E', 8, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (282, N'F', 8, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (283, N'A', 9, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (284, N'B', 9, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (285, N'C', 9, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (286, N'D', 9, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (287, N'E', 9, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (288, N'F', 9, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (289, N'A', 10, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (290, N'B', 10, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (291, N'C', 10, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (292, N'D', 10, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (293, N'E', 10, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (294, N'F', 10, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (295, N'A', 11, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (296, N'B', 11, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (297, N'C', 11, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (298, N'D', 11, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (299, N'E', 11, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (300, N'F', 11, 5)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (301, N'A', 1, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (302, N'B', 1, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (303, N'C', 1, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (304, N'D', 1, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (305, N'E', 1, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (306, N'F', 1, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (307, N'A', 2, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (308, N'B', 2, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (309, N'C', 2, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (310, N'D', 2, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (311, N'E', 2, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (312, N'F', 2, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (313, N'A', 3, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (314, N'B', 3, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (315, N'C', 3, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (316, N'D', 3, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (317, N'E', 3, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (318, N'F', 3, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (319, N'A', 4, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (320, N'B', 4, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (321, N'C', 4, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (322, N'D', 4, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (323, N'E', 4, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (324, N'F', 4, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (325, N'A', 5, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (326, N'B', 5, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (327, N'C', 5, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (328, N'D', 5, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (329, N'E', 5, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (330, N'F', 5, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (331, N'A', 6, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (332, N'B', 6, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (333, N'C', 6, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (334, N'D', 6, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (335, N'E', 6, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (336, N'F', 6, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (337, N'A', 7, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (338, N'B', 7, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (339, N'C', 7, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (340, N'D', 7, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (341, N'E', 7, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (342, N'F', 7, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (343, N'A', 8, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (344, N'B', 8, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (345, N'C', 8, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (346, N'D', 8, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (347, N'E', 8, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (348, N'F', 8, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (349, N'A', 9, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (350, N'B', 9, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (351, N'C', 9, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (352, N'D', 9, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (353, N'E', 9, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (354, N'F', 9, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (355, N'A', 10, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (356, N'B', 10, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (357, N'C', 10, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (358, N'D', 10, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (359, N'E', 10, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (360, N'F', 10, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (361, N'A', 11, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (362, N'B', 11, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (363, N'C', 11, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (364, N'D', 11, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (365, N'E', 11, 6)
GO
INSERT [dbo].[cho_ngoi] ([id], [cot], [hang], [id_phong_chieu]) VALUES (366, N'F', 11, 6)
GO
SET IDENTITY_INSERT [dbo].[cho_ngoi] OFF
GO
SET IDENTITY_INSERT [dbo].[khach_hang] ON 
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (1, N'Hà Nội', 1000, N'1', N'121432136', N'Khách hàng 1')
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (2, N'Hà Nội', 100, N'2', N'123456545', N'Khách hàng 2')
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (3, N'Hà Nội', 1, N'3', N'2168787887', N'Khách hàng 3')
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (5, N'Hà Nội', 0, N'4', N'374638763732', N'Khách hàng 4')
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (6, N'Hà Nội', 1000000, N'5', N'32768736873', N'Khách hàng 5')
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (7, N'Hà Nội', 100, N'6', N'3298379992', N'Khách hàng 6')
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (8, N'Hà Nội', 100, N'7', N'32679873923', N'Khách hàng 7')
GO
INSERT [dbo].[khach_hang] ([id], [dai_chi], [diem], [ma_the_than_thiet], [so_dien_thoai], [ten]) VALUES (9, N'Hà Nội', 55, N'8', N'32679873923', N'Khách hàng 8')
GO
SET IDENTITY_INSERT [dbo].[khach_hang] OFF
GO
SET IDENTITY_INSERT [dbo].[khung_gio] ON 
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (1, N'ok', N'08:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (2, N'ok', N'09:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (3, N'ok', N'10:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (4, N'ok', N'11:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (5, N'ok', N'12:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (6, N'ok', N'13:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (7, N'ok', N'14:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (8, N'ok', N'15:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (9, N'ok', N'16:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (10, N'ok', N'20:00')
GO
INSERT [dbo].[khung_gio] ([id], [mo_ta], [ten]) VALUES (11, N'ok', N'18:00')
GO
SET IDENTITY_INSERT [dbo].[khung_gio] OFF
GO
SET IDENTITY_INSERT [dbo].[nhan_vien] ON 
GO
INSERT [dbo].[nhan_vien] ([id], [chuc_vu], [cmt], [dai_chi], [mat_khau], [ngay_sinh], [so_dien_thoai], [tai_khoan], [ten]) VALUES (1, N'Admin', N'26738216387621', N'Hà Nội', N'123456', CAST(N'1999-05-30' AS Date), N'0398092226', N'sonha', N'Đinh Sơn Hà')
GO
INSERT [dbo].[nhan_vien] ([id], [chuc_vu], [cmt], [dai_chi], [mat_khau], [ngay_sinh], [so_dien_thoai], [tai_khoan], [ten]) VALUES (4, N'salesman', N'22132121', N'HN', N'123456', CAST(N'1999-05-30' AS Date), N'21876382768', N'nhanvien1', N'Nhân Viên 1')
GO
INSERT [dbo].[nhan_vien] ([id], [chuc_vu], [cmt], [dai_chi], [mat_khau], [ngay_sinh], [so_dien_thoai], [tai_khoan], [ten]) VALUES (5, N'salesman', N'324324324', N'TB', N'123456', CAST(N'1999-05-30' AS Date), N'23213213213', N'nhanvien2', N'Nhân viên 2')
GO
INSERT [dbo].[nhan_vien] ([id], [chuc_vu], [cmt], [dai_chi], [mat_khau], [ngay_sinh], [so_dien_thoai], [tai_khoan], [ten]) VALUES (9, N'manager', N'21231232121', N'TB', N'123456', CAST(N'1999-05-30' AS Date), N'324324324', N'quanly', N'Quản Lý')
GO
SET IDENTITY_INSERT [dbo].[nhan_vien] OFF
GO
SET IDENTITY_INSERT [dbo].[phim] ON 
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (1, N'END GAME', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (2, N'Đề thi đẫm máu', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (3, N'Bản thông báo tử vong', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (4, N'Tội lỗi phi chứng cứ', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (5, N'Conan', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (6, N'Thượng đế cũng phải cười', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (7, N'No code no life', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (8, N'Hacker nón lá', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (9, N'Ông it học cơ khí bách khoa', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (10, N'300 bài code thiếu nhi', N'Hành Động', 120)
GO
INSERT [dbo].[phim] ([id], [ten], [the_loai], [thoi_luong]) VALUES (11, N'PTTK&DBCLPM', N'Hành Động', 120)
GO
SET IDENTITY_INSERT [dbo].[phim] OFF
GO
