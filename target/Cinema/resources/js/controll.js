$(document).ready(function() {
    $("#home-link").click(function(e) {
        $.get({
            url: "./home",
            success: function(e) {
                window.location.href = "./home";
            }
        });
    });
    $(".sider-menu").click(function(e) {
        $(".sider-menu").removeClass("bg-secondary text-white");
        $(this).addClass("bg-secondary text-white");
    });
    $("#ticket-selling-link").click(function(e) {
        $.get({
            url: "./sell",
            success: function(response) {
                $("#content").html(response);
            }
        });
    });
    $("#room-link").click(function(e) {
        $.get({
            url: "./room",
            success: function(response) {
                $("#content").html(response);
            }
        });
    });
    $("#schedule").click(function(e) {
        $.get({
            url: "./schedule",
            success: function(response) {
                $("#content").html(response);
            }
        });
    });
});