$(document).ready(function() {
	$("#btnLogin").click(function(e) {
		var username = $("#username").val();
		var password = $("#password").val();
		$.post({
			url: "./login",
			data: {
				username: username,
				password: password
			},
			success: function(e) {
				window.location.href = "./home";
			},
			error: function(e) {
				$("#mess").html("<h6 class='text-center mt-4 text-danger'>Sai tài khoản hoặc mật khẩu!</h6>");
			}
		});

	});
});