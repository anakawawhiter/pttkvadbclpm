$(document).ready(function() {
    $("#showtime-by-room").click(function(e) {
        var roomId = $("#selected-room").val();
        console.log(roomId);
        $.post({
            url: "./showtimeByRoom",
            data: {
                roomId: roomId
            },
            success: function(response) {
                $("#time-show").html(response);
            }
        });
    });
    $("#showtime-by-film").click(function(e) {
        var filmId = $("#selected-film").val();
        console.log(filmId);
        $.post({
            url: "./showtimeByFilm",
            data: {
                filmId: filmId
            },
            success: function(response) {
                $("#time-show").html(response);
            }
        });
    });
});