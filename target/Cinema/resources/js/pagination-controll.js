$(document).ready(function() {
	var filmId = $("#selected-film").val();
	var roomId = $("#selected-room").val();
	var curentPage = $(".page-item.active").children().text() - 0;
	var totalPages = $("#totalPage").val() - 0;
	if (curentPage == 1) {
		$("#prePage").addClass("disabled btn-secondary");
	}
	if (curentPage == totalPages) {
		$("#nextPage").addClass("disabled btn-secondary");
	}
	$(".page-link").click(function(e) {
		var oldPage = $(".page-item.active").children().text() - 0;
		var newPage = $(this).text() - 0;
		var totalPage = $("#totalPage").val() - 0;
		$.post({
			url: "./showtimePaging",
			data: {
				oldPage: oldPage,
				newPage: newPage,
				totalPage: totalPage,
				filmId: filmId,
				roomId: roomId

			},
			success: function(response) {
				$("#time-show").html(response);
			}
		});
	});
	$("#prePage").click(function(e) {
		var oldPage = $(".page-item.active").children().text() - 0;
		if (oldPage > 1) {
			var newPage = 0 + oldPage - 1;
			var totalPage = $("#totalPage").val() - 0;
			$.post({
				url: "./showtimePaging",
				data: {
					oldPage: oldPage,
					newPage: newPage,
					totalPage: totalPage,
					filmId: filmId,
					roomId: roomId

				},
				success: function(response) {
					$("#time-show").html(response);
				}
			});
		}
	});
	$("#nextPage").click(function(e) {
		var oldPage = $(".page-item.active").children().text() - 0;
		var totalPage = $("#totalPage").val() - 0;
		if (oldPage < totalPage) {
			var newPage = oldPage + 1;
			$.post({
				url: "./showtimePaging",
				data: {
					oldPage: oldPage,
					newPage: newPage,
					totalPage: totalPage,
					filmId: filmId,
					roomId: roomId

				},
				success: function(response) {
					$("#time-show").html(response);
				}
			});
		}
	});
	$(".schedule-link").click(function(e) {
		var scheduleId = $(this).attr("id");
		$.post({
			url: "./selectSeat",
			data: {
				scheduleId: scheduleId
			},
			success: function(response) {
				$("#content").html(response);
			}
		});
	});
});