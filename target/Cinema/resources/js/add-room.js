$(document).ready(function() {
    var cinemaRoom;
    var seatQuantity;
    $("#addNewBtn").click(function(e) {
        $("#modalAddRoom").modal('show');

    });
    $("#add-room").click(function(e) {
        cinemaRoom = $("#cinemaRoom").val();
        seatQuantity = $("#seatQuantity").val();
        console.log("Run")
        if (validateFormAdd() == true) {
            $("#modalAddRoom").modal('hide');
            $.post({
                url: "./room/add",
                data: {
                    ten: cinemaRoom,
                    soGhe: seatQuantity
                },
                success: function(response) {
                    $("#content").html(response);
                    showSuccessAlert();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    showSuccessAlert();
                }
            });
        }
    });

    function validateFormAdd() {
        var check = true;
        if (cinemaRoom == "") {
            $("#messCinemaRoom").html("This field is mandatory");
            $("#messCinemaRoom").css("color", "red");
            check = false;
        } else {
            $("#messCinemaRoom").html("Valid");
            $("#messCinemaRoom").css("color", "green");
        }
        $("#messSeatQuantity").html("Valid");
        $("#messSeatQuantity").css("color", "green");
        if (seatQuantity == "" || seatQuantity == 0) {
            $("#messSeatQuantity").html("This field is mandatory");
            $("#messSeatQuantity").css("color", "red");
            check = false;
        } else {
            if ((seatQuantity % 6) != 0) {
                $("#messSeatQuantity").html("This field must be divided by 6 ");
                $("#messSeatQuantity").css("color", "red");
                check = false;
            }
            if (seatQuantity < 24) {
                $("#messSeatQuantity").html("This field must be greater than or equal to 24 ");
                $("#messSeatQuantity").css("color", "red");
                check = false;
            }
            if (seatQuantity > 120) {
                $("#messSeatQuantity").html("This field must be smaller than or equal to 120");
                $("#messSeatQuantity").css("color", "red");
                check = false;
            }
        }

        if (check == true) {
            return true;
        } else {
            return false;
        }
    }

    function showSuccessAlert() {
        $("#success-alert").fadeTo(1000, 1000).slideUp(1000, function() {
            $("#success-alert").slideUp(1000);
        });
    }
});