$(document).ready(function() {
    $(".seat").click(function(e) {
        var classString = $(this).attr("class");
        var classList = classString.split(/\s+/);
        var checkSelected = false;
        for (i = 0; i < classList.length; i++) {
            if (classList[i] == "btn-success") {
                checkSelected = true
                break;
            }
        }
        if (checkSelected == true) {
            $(this).removeClass("btn-success");
            $(this).addClass("btn-light");
        } else {
            $(this).addClass("btn-success");
            $(this).removeClass("btn-light");
        }
    });
    $("#book").click(function(e) {
        var eleList = $(".btn-success.seat");
        var listId = [];
        $.map(eleList, function(elementOrValue, indexOrKey) {
            listId.push(elementOrValue.id);
        });
        console.log(listId);
        var roomId = $(".room").attr("id");
        $.post({
            url: "./comfirmTicket",
            data: {
                id: roomId,
                listSeatId: listId
            },
            success: function(response) {
                $(".content").html(response);
            },
        });
    });
    $("#back").click(function(e) {
        $.get({
            url: "./sell",
            success: function(response) {
                $("#content").html(response);
            }
        });
    });
});