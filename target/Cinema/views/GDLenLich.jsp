<%--
  Created by IntelliJ IDEA.
  User: sonha
  Date: 20-Nov-21
  Time: 5:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="/common/taglib.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<div class="list-film col-10 col-lg-10 col-md-10 col-sm-12">
    <div class="h3 row text-center">Lên lịch</div>
    <div class="row">
        <div class="col-12">
            <form action="" class="form">
                <div class="form-outline mb-4">
                    <input type="text" id="form3Example1q" class="form-control"/>
                    <label class="form-label" for="form3Example1q">Name</label>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-4">

                        <div class="form-outline datepicker">
                            <input
                                    type="text"
                                    class="form-control"
                                    id="exampleDatepicker1"
                            />
                            <label for="exampleDatepicker1" class="form-label">Select a date</label>
                        </div>
                    </div>
                    <div class="col-md-6 mb-4">

                        <select class="select">
                            <option value="1" disabled>Gender</option>
                            <option value="2">Female</option>
                            <option value="3">Male</option>
                            <option value="4">Other</option>
                        </select>

                    </div>
                </div>
                <div class="mb-4">
                    <select class="select">
                        <option value="1" disabled>Class</option>
                        <option value="2">Class 1</option>
                        <option value="3">Class 2</option>
                        <option value="4">Class 3</option>
                    </select>
                </div>
                <div class="row mb-4 pb-2 pb-md-0 mb-md-5">
                    <div class="col-md-6">

                        <div class="form-outline">
                            <input type="text" id="form3Example1w" class="form-control"/>
                            <label class="form-label" for="form3Example1w">Registration code</label>
                        </div>

                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-lg mb-1">Submit</button>
            </form>
        </div>
    </div>
</div>