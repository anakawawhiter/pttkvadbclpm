<%@page import="sonha.com.model.HoaDon"%>
<%@page import="org.springframework.format.Printer"%>
<%@page import="java.sql.Date"%>
<%@page import="sonha.com.model.Phim"%>
<%@page import="sonha.com.model.KhungGio"%>
<%@page import="sonha.com.model.PhongChieu"%>
<%@page import="sonha.com.model.RapPhim"%>
<%@page import="sonha.com.model.Ve"%>
<%@page import="java.util.List"%>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="table-responsive col-8 offset-2">
	<span class="h3">Chi tiết hoá đơn</span>
	<%
	HoaDon hoaDon = (HoaDon) request.getAttribute("hoaDon");
	List<Ve> ves = hoaDon.getVes();
	for (int i = 0; i < ves.size(); i++) {
		Ve ve = ves.get(i);
		out.println("<p>" + "Vé :" + i + 1 + ", Rạp phim: " + ve.getRapPhim().getTen() + ", Phim:"
		+ ve.getChoNgoiLichChieu().getLichChieu().getPhim().getTen() + ", Ngày: "
		+ ve.getChoNgoiLichChieu().getLichChieu().getNgayChieu() + ", Khung giờ: "
		+ ve.getChoNgoiLichChieu().getLichChieu().getKhungGio().getTen() + ", Phòng: "
		+ ve.getChoNgoiLichChieu().getLichChieu().getPhongChieu().getTen() + ", chỗ ngồi:"
		+ ves.get(i).getChoNgoiLichChieu().getChoNgoi().getHang()
		+ ves.get(i).getChoNgoiLichChieu().getChoNgoi().getCot() + ", Giá vé:"
		+ ves.get(i).getThanhTien() + "</p>");
	}
	out.println("<p>" + "Tong Tien: " + hoaDon.getTongTien() + "</p>");
	out.println("<p>" + "Ngươi bán: " + hoaDon.getNguoiBan().getTen() + "</p>");
	out.println("<p>" + "ngày xuất: " + hoaDon.getThoiGian() + "</p>");
	%>
	<div class="d-flex flex-row-reverse">
		<button class="btn btn-primary " id="ok">Ok</button>
	</div>

</div>
<script src='<c:url value="/resources/js/done.js" />'></script>