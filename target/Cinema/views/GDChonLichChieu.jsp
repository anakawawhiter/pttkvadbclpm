<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<c:forEach items="${lichChieus}" var="lich" varStatus="index">
	<div class="row ">
		<a href="#" class="item d-flex schedule-link" id="${lich.id}">
			<div>
				<img src='
			<c:url value="/resources/img/imgcard.png" />
			'
					style="max-width: 100px;" alt="phim">
			</div>
			<div>
				<div>
					<span>Tên phim: ${lich.phim.ten}</span>
				</div>
				<div>
					<Span>Thời lượng: ${lich.phim.thoiLuong}</Span>
				</div>
				<div>
					<span>Giò chiếu: ${lich.khungGio.ten}</span>
				</div>
				<div>
					<span>Phòng chiếu: ${lich.phongChieu.ten}</span>
				</div>
				<div>
					<span>Giá vé: ${lich.gia}</span>
				</div>
				<div>
					<span>Ngày chiếu: ${lich.ngayChieu}</span>
				</div>
			</div>
		</a>
	</div>
	<hr>
</c:forEach>
<div class="d-flex flex-row-reverse">
	<input type="text" class="d-none" value="${totalPage}" name=""
		id="totalPage">
	<nav aria-label="Page navigation example">
		<ul class="pagination">
			<li class="page-item"><a class="page-link btn" href="#"
				id="prePage" aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
			</a></li>
			<%
			Integer total = (Integer) request.getAttribute("totalPage");
			Integer curentPage = (Integer) request.getAttribute("newPage");
			for (int i = 1; i <= total; i++) {
				if (i == curentPage) {
					out.println(
					"<li class=\"page-item active\"><a class=\"page-link btn\" href=\"#\">" + i + "</a></li>");
				} else {
					out.println("<li class=\"page-item\"><a class=\"page-link btn\" href=\"#\">" + i + "</a></li>");
				}

			}
			%>
			<li class="page-item"><a class="page-link btn" href="#"
				id="nextPage" aria-label="Next"> <span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
			</a></li>
		</ul>
	</nav>
	<script src='<c:url value="/resources/js/pagination-controll.js" />'></script>
</div>
