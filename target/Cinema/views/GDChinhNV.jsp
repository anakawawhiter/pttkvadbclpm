<%@include file="/common/taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!doctype html>
<html lang="en">

<head>
    <title>Home</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <nav
                class="navbar navbar-expand-sm bg-dark col-12 col-sm-12 col-lg-12 col-md-12 justify-content-between">
            <div class="d-flex navbar-nav">
                <div class="nav-item">
                    <a href="#" id="home-link" class="navbar-brand text-white">SohaPhim</a>
                </div>
                <form class="form-inline" action="/action_page.php">
                    <input class="form-control mr-sm-2" type="text"
                           placeholder="Nhập tên phim">
                    <button class="btn btn-success" type="submit">Tìm phim</button>
                </form>

                <div class="nav-item">
                    <a href="choosefilmandtime.html" class="nav-link text-white">Lịch
                        chiếu</a>
                </div>
            </div>
            <div class="d-flex navbar-nav">
                <div class="nav-item">
						<span class="btn text-white ">Welcome
                            ${nhanVienLogined.taiKhoan}</span>
                </div>

                <div class="nav-item">
                    <a href="${pageContext.request.contextPath}/logout"
                       class="btn text-white">Đăng xuất</a>
                </div>
            </div>
        </nav>
    </div>
    <br>
    <div class="row">
        <div
                class=" col-2 col-lg-2 col-md-2 col-sm-12 border-right text-center">
            <img src='<c:url value="/resources/img/sonha.jpg" />'
                 class="img-thumbnail rounded-circle " style="max-width: 200px;"
                 alt=""> <br> <span class="font-weight-bolder">${nhanVienLogined.ten}</span>
            <br>
            <hr>
            <a id="schedule" class="card btn sider-menu" href="#">
                Lên lịch </a> <br> <a class="card btn sider-menu "
                                      href="#" id="ticket-selling-link"> Bán vé </a> <br> <a
                class="card btn sider-menu" href="#" id="service-link"> Dịch vụ
        </a> <br> <a class="card btn sider-menu" href="#" id="room-link">
            Phòng chiếu </a> <br> <a class="card  btn sider-menu" href="#"
                                     id="stat-link"> Thóng kê </a> <br>
        </div>
        <div id="content"
             class="container col-10 col-lg-10 col-md-10 col-sm-12">
            <div class="list-film col-12 col-lg-12 col-md-12 col-sm-12 row">
                <div class="list-film col-12 col-lg-12 col-md-12 col-sm-12 row">
                    <div
                            class="item col-3 col-lg-3 col-md-3 col-sm-12 justify-content-center text-center">
                        <a href="http://"><img
                                src='<c:url value="/resources/img/imgcard.png" />'
                                class="img-thumbnail" alt=""></a> <a href="http://"
                                                                     class="text-primary font-weight-bolder">End
                        game</a>
                    </div>
                    <div
                            class="item col-3 col-lg-3 col-md-3 col-sm-12 justify-content-center text-center">
                        <a href="http://"><img
                                src='<c:url value="/resources/img/imgcard.png" />'
                                class="img-thumbnail" alt=""></a> <a href="http://"
                                                                     class="text-primary font-weight-bolder">End
                        game</a>
                    </div>
                    <div
                            class="item col-3 col-lg-3 col-md-3 col-sm-12 justify-content-center text-center">
                        <a href="http://"><img
                                src='<c:url value="/resources/img/imgcard.png" />'
                                class="img-thumbnail" alt=""></a> <a href="http://"
                                                                     class="text-primary font-weight-bolder">End
                        game</a>
                    </div>
                    <div
                            class="item col-3 col-lg-3 col-md-3 col-sm-12 justify-content-center text-center">
                        <a href="http://"><img
                                src='<c:url value="/resources/img/imgcard.png" />'
                                class="img-thumbnail" alt=""></a> <a href="http://"
                                                                     class="text-primary font-weight-bolder">End
                        game</a>
                    </div>
                    <div
                            class="item col-3 col-lg-3 col-md-3 col-sm-12 justify-content-center text-center">
                        <a href="http://"><img
                                src='<c:url value="/resources/img/imgcard.png" />'
                                class="img-thumbnail" alt=""></a> <a href="http://"
                                                                     class="text-primary font-weight-bolder">End
                        game</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <br>
    <hr>
    <div class="row">
        <footer class="col-12 col-md-12 col-sm-12 col-lg-12 text-center">
            <Span class="text-secondary text-center">&reg;SonHa&trade;</Span>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse
                perferendis atque, odit molestias blanditiis asperiores, voluptate
                placeat hic quos molestiae ipsam distinctio laboriosam temporibus
                saepe ut facilis sed fugit voluptates!</p>
        </footer>
    </div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src='<c:url value="/resources/js/controll.js" />'></script>
</body>

</html>