<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<table class="table table-light">
	<tr>
		<td>Tên khách</td>
		<td>${khachHang.ten}</td>
	</tr>
	<tr>
		<td>Số điện thoại</td>
		<td>${khachHang.soDienThoai}</td>
	</tr>
	<tr>
		<td>Điểm hiện có</td>
		<td id="point">${khachHang.diem}</td>
	</tr>
	<tr>
		<td>Có muốn đổi điểm?</td>
		<td><button id="exchange-btn" class="btn btn-success">ok</button></td>
	</tr>
	<tr>
		<td>điểm còn:</td>
		<td id="surplus"></td>
	</tr>
	<!--  <tr>
		<td><span class="text-danger" id="notify"></span></td>
		<td><input type="text" id="idCustomer" value="${khachHang.id}"> </td>
	</tr>-->
	<script src='<c:url value="/resources/js/exchange-point.js" />'></script>
</table>