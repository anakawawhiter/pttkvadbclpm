<%@page import="org.springframework.format.Printer"%>
<%@page import="java.sql.Date"%>
<%@page import="sonha.com.model.Phim"%>
<%@page import="sonha.com.model.KhungGio"%>
<%@page import="sonha.com.model.PhongChieu"%>
<%@page import="sonha.com.model.RapPhim"%>
<%@page import="sonha.com.model.Ve"%>
<%@page import="java.util.List"%>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="table-responsive col-8 offset-2">
	<table class="table table-light">
		<thead>
			<tr>
				<td colspan="2" class="h1">Xác nhận vé</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="h5">Tên rạp</td>
				<%
				RapPhim rapPhim = (RapPhim) request.getAttribute("rapPhim");
				out.print("<td>" + rapPhim.getTen() + "</td>");
				%>
			</tr>
			<tr>
				<td class="h5">Phòng chiếu</td>
				<%
				PhongChieu phongChieu = (PhongChieu) request.getAttribute("phongChieu");
				out.print("<td>" + phongChieu.getTen() + "</td>");
				%>
			</tr>
			<tr>
				<td class="h5">Khung giờ</td>
				<%
				KhungGio khungGio = (KhungGio) request.getAttribute("khungGio");
				out.print("<td>" + khungGio.getTen() + "</td>");
				%>
			</tr>
			<tr>
				<td class="h5">Ngày chiếu</td>
				<%
				Date ngayChieu = (Date) request.getAttribute("ngayChieu");
				out.print("<td>" + ngayChieu + "</td>");
				%>
			</tr>
			<tr>
				<td class="h5">phim</td>
				<%
				Phim phim = (Phim) request.getAttribute("phim");
				out.print("<td>" + phim.getTen() + "</td>");
				%>
			</tr>
			<tr>
				<td colspan="2" class="h5">Ghế đã chọn chọn: </td>
			</tr>
			<%
			List<Ve> ves = (List<Ve>) request.getAttribute("ves");
			for (int i = 0; i < ves.size(); i++) {
				out.println("<tr><td>" + ves.get(i).getChoNgoiLichChieu().getChoNgoi().getHang()
				+ ves.get(i).getChoNgoiLichChieu().getChoNgoi().getCot() + "</td><td class=\"money\">"
				+ ves.get(i).getThanhTien() + "</td></tr>");
			}
			out.println("<input type=\"text\" style=\" display: none;\" id=\"amount\" value=\""+ves.size()+"\">");
			%>
		</tbody>

	</table>
	<div>
		<form class="form-inline">
			<input class="form-control mr-sm-1" type="text" id="customer-code"
				placeholder="Nhập mã khách hàng thân thiết">
			<button class="btn btn-success" type="button" id="btn-search-cus">Tìm</button>
			<span class="text-danger" id="messErr"></span>
			<script src='<c:url value="/resources/js/search-customer.js" />'></script>
		</form>
	</div>
	<div id="score-exchange"></div>
	<table class="table table-light">
		<tr>
			<td>Tổng tiền</td>
			<td><span id="total"></span></td>
		</tr>
	</table>
	<div class="d-flex flex-row-reverse">
		<button class="btn btn-success" type="button" id="checkout">Xuất
			hoá đơn và in vé</button>
	</div>
	<script src='<c:url value="/resources/js/total.js" />'></script>
</div>
<script src='<c:url value="/resources/js/invoice-export.js" />'></script>